const erro = (msg) => {
  const mensagemErro = document.getElementById("mensagem-erro");
  mensagemErro.innerHTML = msg;
};

const validaCampos = (event) => {
  event.preventDefault();
  const nome_usuario = document.getElementById("nome-usuario");

  if (nome_usuario.value === "") {
    erro("Nome não informado");
    nome_usuario.focus();
  }
};
